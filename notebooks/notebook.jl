### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ b49af327-9a98-4975-9f24-0fecf434c4cd
using Gadfly, CSV, DataFrames, Pipe, Colors

# ╔═╡ eeecfbec-e51d-11eb-238c-2f1e619b8153
data = CSV.read("../data/processed/sms-vd.csv", DataFrame)

# ╔═╡ 7ebad153-9d8d-4df1-a7a0-e4945196495e
Gadfly.with_theme(:dark) do
	plot(data,
		x=:intentional_homicide_rate2018,
		y=:violent_deaths_by_firearm_rate2018,
		color=:region, label=:country_code, Geom.point, Geom.label(position=:dynamic),
		Guide.xlabel("Homicidios intencionales"),
		Guide.ylabel("Muertes violentas con armas de fuego"),
		Guide.colorkey(title="Region"))
end

# ╔═╡ Cell order:
# ╠═b49af327-9a98-4975-9f24-0fecf434c4cd
# ╠═eeecfbec-e51d-11eb-238c-2f1e619b8153
# ╠═7ebad153-9d8d-4df1-a7a0-e4945196495e
