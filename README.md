# Small arms survey data on violent deaths


The Small Arms Survey's Global Violent Deaths (GVD) Database, 2018 (rev. 2020 Aug)

Todos los datos fueron descargados de

Update: 31/03/2020
Disclaimer and Notes:
Data collection closed at 15/11/2019
Estimates are based on research conducted by the Small Arms Survey. They do not necessarily imply the endorsement of any state.
Population rates are cited from World Population Prospects: The 2019 Revision (UNDESA, 2019).
The composition of sub-regions is based on the classification system used by the UN Statistical Division (UNSD, October 2013 revision). The boundaries, country names, and designations on these maps do not imply the endorsement of the Small Arms Survey.